git config --global alias.co checkout
git config --global alias.l 'log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=short'
git config --global alias.a 'add'
git config --global alias.aa 'add -A'
git config --global alias.c 'commit --verbose'
git config --global alias.ci 'commit -a --verbose'
git config --global alias.d 'diff'
git config --global alias.s status
git config --global alias.cob 'checkout -b'
git config --global alias.sb "!git for-each-ref --sort='-authordate' --format='%(authordate)%09%(objectname:short)%09%(refname)' refs/heads | sed -e 's-refs/heads/--'"
git config --global alias.pushitgood 'push -u origin --all'
git config --global alias.rao 'remote add origin'
git config --global alias.ac '!git add . && git commit -am'
